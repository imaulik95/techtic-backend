<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
	static $rules = [
        'name' => 'required',
        'unique_code' => 'required|unique:products',
        'description' => 'required',
        'category' => 'required',
    ];
    protected $fillable = [
        'name',
        'unique_code',
        'description',
        'category_id',

    ];
    public function category()
    {
        return $this->belongsTo(ProductCatrgory::class, 'category_id', 'id');
    }
}
