<?php

namespace App\Imports;

use App\Product;
use App\ProductCatrgory;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class ProductImport implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        DB::beginTransaction();
        Validator::make($rows->toArray(), [
            '*.name' => 'required',
            '*.category' => 'required',
            '*.description' => 'required',
            '*.unique_code' => 'required',
        ])->validate();
        DB::rollback();
        foreach ($rows as $row) {
            	$productCat = ProductCatrgory::create(['name' => $row['category']]);	
            	if($productCat)
				{
                	$product = Product::create([
	                    'name' => $row['name'],
	                    'description' => $row['description'],
	                    'unique_code' => $row['unique_code'],
	                    'category_id' => $productCat->id,
                	]);
            	}
                DB::commit();
            
        }
    }
}
