<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCatrgory extends Model
{
   
    static $rules = [
        'name' => 'required|unique:product_catrgories',
    ];
    protected $fillable = [
        'name',

    ];
}
