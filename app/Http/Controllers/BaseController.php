<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function formatSuccessResponse($msg, $data, $code = 200)
    {
        $response = [
            'status' => 'success',
            'code' => $code,
            'message' => $msg,
            'data' => $data
        ];
        return response()->json($response, $code);
    }

    public function formatFailureResponse($msg, $code = 500)
    {
        $response = [
            'status' => 'failure',
            'code' => $code,
            'message' => $msg
        ];
        return response()->json($response, $code);
    }

    public function jSonFormatSuccessResponse($data, $code = 200)
    {
        return response()->json($data, $code);
    }

    public function jSonFormatFailureResponse($msg, $code = 500)
    {
        return response()->json($msg, $code);
    }

    public function apiFormatSuccessResponse($msg, $itmsg, $data, $code = 200)
    {
        $response = [
            'status' => 'success',
            'code' => $code,
            'msg' => $msg,
            'itmsg' => $itmsg,
            'data' => $data
        ];
        return response()->json($response, $code);
    }

    public function apiFormatFailureResponse($msg, $itmsg, $code = 500)
    {
        $response = [
            'status' => 'unsuccess',
            'code' => $code,
            'message' => $msg,
            'errors' => $itmsg
        ];
        return response()->json($response, $code);
    }
}
