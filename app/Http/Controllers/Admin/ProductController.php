<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use App\Product;
use App\ProductCatrgory;
use Dotenv\Exception\ValidationException;
use Illuminate\Support\Facades\Auth;
use App\Imports\ProductImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
class ProductController extends BaseController
{
	public function product_list(Request $request)
    {
        return datatables(Product::with('category'))->toJson();
    }
    public function importProduct(Request $request)
    {
        if ($request->hasFile('import_file')) {
            try {
                $data = Excel::import(new ProductImport, $request->file('import_file'));
            } catch (\Exception $e) {
                return $this->apiFormatFailureResponse('something went wrong in file data', ['category must be unique or', 'unique code must be unique']);
            }
            if (!empty($data)) {
                return $this->formatSuccessResponse('import successfully', $data);
            }
            return $this->apiFormatFailureResponse('issue', $data);
        }
    }
}
