<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }
        $user = Auth::user();
        $token = $user->createToken('Laravel');
        $user->api_token = $token->accessToken;
        $user->save();
        $data = [
            'user' => $user,
            'access_token' =>$token->accessToken
        ];
        return $this->formatSuccessResponse('user login success', $data);

    }
    public function register(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email:rfc,dns|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        $requestData = $request->all();
        $requestData['password'] = Hash::make($request->input('password'));
        $user = User::create($requestData);
        return $this->formatSuccessResponse('success', $user);
    }
}
