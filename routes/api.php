<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Admin'], function () {
    Route::post('login', 'AuthController@login');
   

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('product/list', 'ProductController@product_list');
        Route::post('product-import-file', 'ProductController@importProduct');
    
    });
});
